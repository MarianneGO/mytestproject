#include "my_test_project.h"
#include <fmt/core.h>

void greet(const std::string& name) {
    fmt::print("Hello there, {}!\n", name);
}